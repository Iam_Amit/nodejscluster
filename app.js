const express = require("express");
const app = express();

const cluster = require("cluster");
const os = require("os");
// const port = 3000;

const numCpu = os.cpus().length;

app.get("/", (req, res) => {
  for (let i = 0; i < 1e8; i++) {}

  res.send(`Ok-------> ${process.pid}`);
  // cluster.worker.kill();
});

if (cluster.isMaster) {
  for (let i = 0; i < numCpu; i++) {
    cluster.fork();
  }
  cluster.on("exist", (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
    cluster.fork();
  });
} else {
  app.listen(3000, () =>
    console.log(`Server ${process.pid} @ http://localhost:3000`)
  );
}

// app.listen(3000, () => console.log(`Server @ http://localhost:3000`));
