const http = require("http");
const cluster = require("cluster");
const numCpus = require("os").cpus().length;
// console.log("numCpus-------->", numCpus); // Will display the number of cpu in the system

if (cluster.isMaster) {
  console.log(`This is the Master process with id ${process.pid}`);
  for (let i = 0; i < numCpus; i++) {
    cluster.fork();
  }
} else {
  //   console.log(`This is the Worker  process with id ${process.pid}`);
  http
    .createServer((req, res) => {
      const message = `This is the Worker  process with id ${process.pid}`;
      console.log(message);
      res.end(message);
    })
    .listen(3000);
}
